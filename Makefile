CC = cc
LIBS = -ljack -lfftw3f -lm -lpthread
CFLAGS = -std=c99 -pedantic -Wall -Werror
LDFLAGS = ${LIBS}
PREFIX = /usr

w2midi: w2midi.o
	${CC} w2midi.o -o w2midi ${LDFLAGS}

w2midi.o: w2midi.c
	${CC} -c ${CFLAGS} w2midi.c -o w2midi.o

clean:
	rm w2midi w2midi.o

uninstall: ${PREFIX}/bin/w2midi
	rm ${PREFIX}/bin/w2midi
	rm ${PREFIX}/share/man/man1/w2midi.1

install: w2midi
	install w2midi -m 755 ${PREFIX}/bin
	install w2midi.1 -m 744 ${PREFIX}/share/man/man1
